# Plotly jupyterlab
---

### Requeriments

    jupyterlab
    plotly==4.14.3
    notebook>=5.3
    ipywidgets>=7.5
    numpy
    pandas
    bokeh
    scipy
    statsmodels
    scikit-learn
    scikit-image
    networkx
    chart-studio
    dash
    jupyter-dash


### Update and upgrade stuff (1)

```bash

[user@machine ~/] $: sudo apt update && sudo apt -y upgrade

```
### Install stuff (2)


```bash

[user@machine ~/] $: sudo apt install -y python3-dev virtualenv virtualenvwrapper

```

### Create a virtual environment (3)


```bash

[user@machine ~/] $: mkvirtualenv amarelinha && cdvirtualenv 

```

### Download the stash (4)

```bash

(amarelinha) [user@machine ~/] $: git clone https://gitlab.com/fredericosales/amarelinha.git && cd amarelinha

```

### Install (5)

```bash

(amarelinha) [user@machine ~/] $: pip install -r requeriments.txt

```

### Install jupyterlab requirements to use plotly

```bash

(amarelinha) [user@machine ~/] $: jupyter labextension install jupyterlab-plotly@4.14.3

```

### Install jupyterlab optional requirements to use plotly

```bash

(amarelinha) [user@machine ~/] $: jupyter labextension install @jupyter-widgets/jupyterlab-manager plotlywidget@4.14.3

```

### Use the sh**t

```bash

(amarelinha) [user@machine ~/] $: jupyter-lab

```

### Use the force and read the source.
---

1. [Plotly docs](https://plotly.com/python/)
2. [Jupyterlab](https://jupyter.org/)
3. [Virtualenv](https://pypi.org/project/virtualenv/)
4. [Virtualenvwrapper](https://pypi.org/project/virtualenvwrapper/)

### Lazy mode
---

Go to pass 4 above and then...

```bash

(amarelinha) [user@machine ~/] $: ./install_urgent.sh

```

__Wait and be lazy.__

### Thanks
---

__To me, myself and I__, and all open source community.
